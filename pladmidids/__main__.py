import sys


for line in sys.stdin:
    line = line.split("\t", 1)[0]
    plasmid = line.split("|", 1)[0]
    print(line + "\t" + plasmid)

