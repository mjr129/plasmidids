PLASMIDIDS - Plasmid IDs
========================

Given accessions of the form

`PLASMID|ACCESSION⟼...`

Where `⟼` is the ***tab*** character, produces a lookup table

`PLASMID|ACCESSION⟼PLASMID`

Suitable for use with ***coinfinder***.

Usage
-----

`plasmidids`

* `STDIN` input file
* `STDOUT` output file

Example
-------

`plasmidids < plasmids.comp > plasmids.tsv`

Meta
----

```ini
author= Martin Rusilowicz
license= GPLv3
date= 2017
workflow-next= coinfinder
host=bitbucket
type=application,cli,script
language=python3
```